function AktuelleSeite()
{  
  var url = location.href;
  var lastslash = url.lastIndexOf("/");
  var dateiname = url.substring(lastslash + 1);
  var formobjekt = document.forms[0].seiten_dropdown;  

  formobjekt.onchange = function()
  {
    NeueSeite(this.value);
  }
  
  var anzahl = formobjekt.options.length;

  for(var i = 0; i < anzahl; i++)
  {
    if(formobjekt.options[i].value == dateiname)
    {
      formobjekt.options[i].selected = true;
      return;
    }
  }
}
      
function NeueSeite(dateiname)
{
  location.href = dateiname;
}

window.onload = AktuelleSeite;
